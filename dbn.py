import numpy as np
from sklearn.cross_validation import train_test_split
from sklearn.metrics import classification_report
from sklearn import datasets
from nolearn.dbn import DBN
import mnist_util


def test():
    training_set, validation_set, test_set = mnist_util.load_data()
    noised_test_set = mnist_util.add_noise(test_set, 0.1)
    images = mnist_util.get_images(noised_test_set)
    mnist_util.plot_10x10_images(images, 'dummy_noised_test_set.pdf')
    
    trainX = np.array(training_set[0])
    trainY = np.array(training_set[1])
    testX = np.array(noised_test_set[0])
    testY = np.array(noised_test_set[1])
    
    
    # 784 input units 
    # 800 in 1st hidden layer
    # 800 in 2nd hidden layer
    # 10 output units 
    dbn = DBN(
    	[trainX.shape[1], 800, 800, 10],
    	learn_rates = 0.3,
    	learn_rate_decays = 0.9,
    	epochs = 1,
    	verbose = 1)
    dbn.fit(trainX, trainY)
    
    preds = dbn.predict(testX)
    with open('dummy_report', 'w') as outfile:
        outfile.write(classification_report(testY, preds))

def train_report(trainX, trainY, testX, testY, report_path = None, 
        hidden1 = 800, hidden2 = 800):
    dbn = DBN(
    	[trainX.shape[1], hidden1, hidden2, 10],
    	learn_rates = 0.3,
    	learn_rate_decays = 0.9,
    	epochs = 10,
    	verbose = 1)
    dbn.fit(trainX, trainY)
    
    preds = dbn.predict(testX)
    if report_path == None:
        print classification_report(testY, preds)
    else:
        with open(report_path, 'w') as outfile:
            outfile.write(classification_report(testY, preds))

if __name__ == '__main__':
    test()
