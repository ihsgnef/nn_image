import os, sys
import numpy as np
import cPickle, gzip
import matplotlib
import matplotlib.pyplot as plt

def load_data():
    f = open('../data/cifar-10/data_batch_1', 'rb')
    dic = cPickle.load(f)
    f.close()
    # each set for example tr is ['data', 'labels', .., ..]
    tr = [dic['data'] / 255.0, dic['labels']]

    f = open('../data/cifar-10/test_batch', 'rb')
    dic = cPickle.load(f)
    f.close()
    # each set for example tr is ['data', 'labels', .., ..]
    ts = [dic['data'] / 255.0, dic['labels']]
    return (tr, ts)

def gaussian_noise(data_set, dev):
    noised_data = []
    noised_data.append([])
    noised_data.append(data_set[1])
    for data in data_set[0]:
        noise = np.random.normal(0, dev, len(data))
        noised_data[0].append(data + noise)
    return noised_data

def get_images(data_set):
    flattened_images = data_set[0]
    # return training_set-size matrices
    images = [np.reshape(c, (32,32,3), order='F') for c in flattened_images]
    return images


def plot_10x10_images(images, path = None):
    fig = plt.figure()
    # images = [image[3:25, 3:25] for image in images]
    for x in xrange(10):
        for y in xrange(10):
            ax = fig.add_subplot(10, 10, 10 * y + x)
            ax.imshow(images[100 + 10 * y + x])
            plt.xticks(np.array([]))
            plt.yticks(np.array([]))
    if path == None:
        plt.show()
    else:
        plt.savefig(path, format = 'pdf')


def main():
    training_set, test_set = load_data()
    noised_training_set = gaussian_noise(training_set, 0.1)
    images = get_images(noised_training_set)
    plot_10x10_images(images, 'cifar.pdf')


if __name__ == '__main__':
    main()
