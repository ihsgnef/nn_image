import os, sys
import numpy as np
import cPickle, gzip
import matplotlib
import matplotlib.pyplot as plt

def load_data():
    f = gzip.open('../data/mnist.pkl.gz', 'rb')
    tr, vl, ts = cPickle.load(f)
    f.close()
    # each set for example tr is [picture array, label array]
    return (tr, ts)

def gaussian_noise(data_set, dev):
    noised_data = []
    noised_data.append([])
    noised_data.append(data_set[1])
    for data in data_set[0]:
        noise = np.random.normal(0, dev, len(data))
        noised_data[0].append(data + noise)
    return noised_data

def prob_noise(data_set, prob):
    noised_data = []
    noised_data.append([])
    noised_data.append(data_set[1])
    for i, data in enumerate(data_set[0]):
        noised_data[0].append([])
        for v in data:
            flag = np.random.binomial(1, prob)
            if flag == 1:
                noised_data[0][i].append(1 - v)
            else:
                noised_data[0][i].append(v)
    return noised_data

def prob_black_noise(data_set, prob):
    noised_data = []
    noised_data.append([])
    noised_data.append(data_set[1])
    for i, data in enumerate(data_set[0]):
        noised_data[0].append([])
        for v in data:
            flag = np.random.binomial(1, prob)
            if flag == 1:
                noised_data[0][i].append(1)
            else:
                noised_data[0][i].append(v)
    return noised_data

def get_images(data_set):
    flattened_images = data_set[0]
    # return training_set-size matrices
    return [np.reshape(f, (-1, 28)) for f in flattened_images]


def plot_10x10_images(images, path = None):
    fig = plt.figure()
    images = [image[3:25, 3:25] for image in images]
    for x in xrange(10):
        for y in xrange(10):
            ax = fig.add_subplot(10, 10, 10 * y + x)
            ax.matshow(images[10 * y + x], cmap = matplotlib.cm.binary)
            plt.xticks(np.array([]))
            plt.yticks(np.array([]))
    if path == None:
        plt.show()
    else:
        plt.savefig(path, format = 'pdf')


def main():
    training_set, test_set = load_data()
    noised_training_set = prob_noise(training_set, 0.1)
    images = get_images(noised_training_set)
    plot_10x10_images(images)


if __name__ == '__main__':
    main()
