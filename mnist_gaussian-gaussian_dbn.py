import os
import numpy as np
import dbn
import mnist_util as util


def mnist():
    test_noise_dev = [0.5, 0.6, 0.7]
    train_noise_dev = [ 0.5, 0.6, 0.7]
    outdir = 'mnist_gaussian-gaussian/'
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    training_set, test_set = util.load_data()
    
    for tdev in test_noise_dev:
        noised_test_set = util.gaussian_noise(test_set, tdev)
        images = util.get_images(noised_test_set)
        util.plot_10x10_images(images, outdir + 'test_set_' + str(tdev) + '.pdf')
        for dev in train_noise_dev:
            noised_training_set = training_set
            if dev > 0:
                noised_training_set = util.gaussian_noise(training_set, dev)
            images = util.get_images(noised_training_set)
            util.plot_10x10_images(images, outdir + 'training_set_' + str(dev) + '.pdf')
            
            
            trainX = np.array(noised_training_set[0])
            trainY = np.array(noised_training_set[1])
            testX = np.array(noised_test_set[0])
            testY = np.array(noised_test_set[1])
            print '***************************'
            print tdev, dev
            print '***************************'
            dbn.train_report(trainX, trainY, testX, testY,
                    outdir + 'report_' +
                    str(tdev) + '_' + str(dev), 800, 800)

            
def main():
    mnist()

if __name__ == '__main__':
    main()
