The code is 4-layer NN on MNIST, using [nolearn](https://github.com/dnouri/nolearn) package.
Type A noise is added to test set, with parameter tdev (deviation in Gaussian, p in Bernoulli).
Type B noise is added to training, with parameter dev.
The types of noise is listed below:

- Gaussian with zero mean and standard deviation dev, called 'gaussian'
- Flip a pixel according to a Bernoulli of probability p, called 'prob'
- Turn a pixel black according to a Bernoulli of probability p, called 'probblack'

The network is specified in dbn.py.
Utils for mnist like adding noise is in mnist\_util.py
Training is in dataset\_testnoise\_trainnoise\_dbn.py, where testnoise and trainnoise are noise types of training set and test set seperately
